import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Observable } from 'rxjs/Observable';
import { ViewContextService } from '../../../services/viewcontext.service';

@Component({
  selector: 'app-n-classes-distribution',
  templateUrl: './n-classes-distribution.component.html',
  styleUrls: ['./n-classes-distribution.component.css']
})
export class NClassesDistributionComponent implements OnInit, OnChanges {
  classesDistribution: any;
  sumClasses: any;
  sumKinderGardens: any;
  @Input() private generalData: any;
  constructor(private dataService: DataService,
    private viewContextService: ViewContextService) { }

  ngOnInit() {
    //this.dataService.getNationalData().subscribe(result => {
    //  if (result && result[0]) {
    //    if (result[0].classesDistribution) {
    //      this.setClassesDistribution(result[0].classesDistribution);
    //    }
    //    if (result[0].sumClasses) {
    //      this.setSumClasses(result[0].sumClasses);
    //    }
    //    if (result[0].sumKinderGardens) {
    //      this.setSumKinderGardens(result[0].sumKinderGardens);
    //    }
    //  }
    //});
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.generalData) {
      this.generalData = this.generalData[0];//todo, mock_data
      if (this.generalData.classesDistribution) {
        this.setClassesDistribution(this.generalData.classesDistribution);
      }
      if (this.generalData.sumClasses) {
        this.setSumClasses(this.generalData.sumClasses);
      }
      if (this.generalData.sumKinderGardens) {
        this.setSumKinderGardens(this.generalData.sumKinderGardens);
      }
    }
  }


  setClassesDistribution(classesDistribution): void {
    //let year = this.viewContextService.year;
    //let list = classesDistribution.filter(x => x.SHANA_LOAZITH === year);
    let columnData = [
      { showInLegend: false, name: "כיתה רגילה", data: classesDistribution.map(x => x.KITOT_RAGIL), color: '#7AB0C1' },
      { showInLegend: false, name: "כיתת חינוך מיוחד", data: classesDistribution.map(x => x.KITOT_MAYUCHAD), color: '#AEDACD' }
    ]
    this.classesDistribution = {
      columnData: columnData,
      title: { text: 'בתי ספר' },
      subtitle: undefined,
      categories: [
        'א',
        'ב',
        'ג',
        'ד',
        'ה',
        'ו',
        'ז',
        'ח',
        'ט',
        'י',
        'יא',
        'יב',
        'יג',
        'יד'

      ],
      yAxis: {
        min: 0,
        title: undefined,
        tickInterval: 1000,
        labels: {
          format: '{value}'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}</b></td><td style="color:{series.color};padding:0;float:right">:{series.name} </td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      }
    }
  }

  setSumClasses(sumClasses): void {
    //let year = this.viewContextService.year;
    //let list = sumClasses.filter(x => x.SHANA_LOAZITH === year);
    let columnData = [
      { showInLegend: true, name: "כיתה רגילה", data: [sumClasses[0].KITOT_RAGIL], color: '#7AB0C1' },
      { showInLegend: true, name: "כיתת חינוך מיוחד", data: [sumClasses[0].KITOT_MAYUCHAD], color: '#AEDACD' }
    ]
    this.sumClasses = {
      columnData: columnData,
      title: {
        text: undefined,
      },
      subtitle: {
        text: undefined,
      },
      categories: [
        'סה"כ כיתות בי"ס'
      ],
      yAxis: {
        min: 0,
        title: 'here',
        tickInterval: 10000,
        labels: {
          format: '{value}'
        }
      },
      legend: {
        title: {
          // text: '<div dir="rtl">checking<div>City and another city<br/>and another city<span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
          style: {
            fontStyle: 'italic',

          }
        },
        rtl: true,
        align: 'center',
        layout: 'horizontal',
        verticalAlign: 'top',
        x: 0,
        y: 0
      },
      series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }, {
        data: [95.6, 54.4, 29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1]
      }],
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}</b></td><td style="color:{series.color};padding:0;float:right">:{series.name} </td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      }
    }
  }

  setSumKinderGardens(sumKinderGardens): void {
    //let year = this.viewContextService.year;
    //let list = sumKinderGardens.filter(x => x.SHANA_LOAZITH === year);
    let columnData = [
      { showInLegend: false, name: "כיתה רגילה", data: [sumKinderGardens[0].KITOT_RAGIL], color: '#7AB0C1' },
      { showInLegend: false, name: "כיתת חינוך מיוחד", data: [sumKinderGardens[0].KITOT_MAYUCHAD], color: '#AEDACD' }
    ]
    this.sumKinderGardens = {
      columnData: columnData,
      title: { text: 'גני ילדים' },
      subtitle: undefined,
      categories: [
        'גן'
      ],
      yAxis: {
        min: 0,
        title: undefined,
        tickInterval: 10000,
        labels: {
          format: '{value}'
        }
      },
      legend: {
        symbolRadius: 0,//Square shape of the legend
        align: 'left',
        verticalAlign: 'top',
        //layout: 'vertical'
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}</b></td><td style="color:{series.color};padding:0;float:right">:{series.name} </td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      }
    }
  }
}
