import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NClassesDistributionComponent } from './n-classes-distribution.component';

describe('NClassesDistributionComponent', () => {
  let component: NClassesDistributionComponent;
  let fixture: ComponentFixture<NClassesDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NClassesDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NClassesDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
