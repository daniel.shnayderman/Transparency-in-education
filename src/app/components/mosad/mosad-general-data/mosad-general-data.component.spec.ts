import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MosadGeneralDataComponent } from './mosad-general-data.component';

describe('MosadGeneralDataComponent', () => {
  let component: MosadGeneralDataComponent;
  let fixture: ComponentFixture<MosadGeneralDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MosadGeneralDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MosadGeneralDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
