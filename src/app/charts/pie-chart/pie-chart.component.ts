import { Component, SimpleChanges } from '@angular/core';
import { BaseChartComponent } from '../base-chart/base-chart.component';
import { ColumnChartComponent } from '../column-chart/column-chart.component';

@Component({
  selector: 'pie-chart',
  template: '<div [id]="id" [class]="cssclass"></div>',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent extends BaseChartComponent {
  setData(): void {
    throw new Error("Method not implemented.");
  }

  constructor() { super(); }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.chart) {
      this.chart.series[0].setData(this.data);
    }
  }

  chartOptions1(): any {
    return {
      chart: {
        type: 'doughnut',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        },
        animation: true
      },
      title: {
        text: this.title
      },
      credits: {
        enabled: false
      },
      legend: {
        labelFormat: '{name} {percentage:.1f}% - ({y})',
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'right',
        x: 0,
        y: 50,
        rtl: true
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          depth: 35,
          showInLegend: true
        }
      },
      series: [{
        name: this.title,
        data: null
      }]
    };
  }

  chartOptions(): any {
    return {
      chart: {
        animation: false,
        type: 'pie',
        backgroundColor: null
      },
      title: {
        text: '',
        // style:{
        //   color:'blue'
        // }
      },
      tooltip: {
        valueSuffix: '',
        enabled: true
      },
      plotOptions: {
        pie: {
          animation: {
            duration: 750,
            easing: 'easeOutQuad'
          },
          shadow: false,
          center: ['50%', '50%'],
          cursor: 'pointer',
          dataLabels: {
            enabled: false,
            style:{
              color: 'red',
              fontSize: '18px',
            }
          },
        },
        series: {
          animation: {
            duration: 750,
            easing: 'easeOutQuad'
          }
        }
      },
      legend:{
        align:'center',
        backgroundColor: null
      },
      series: [{
        animation: {
          duration: 750,
          easing: 'easeOutQuad'
        },
        name: 'Spending',
        data: null,
        size: '100%',
        innerSize: '85%',
        dataLabels: {
          formatter: function () {
            return this.y > 5 ? this.point.name : null;
          },
          color: '#ffffff',
          distance: 0
        }
      }]
    }
  }





}
