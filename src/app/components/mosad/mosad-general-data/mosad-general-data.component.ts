import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-mosad-general-data',
  templateUrl: './mosad-general-data.component.html',
  styleUrls: ['./mosad-general-data.component.css']
})
export class MosadGeneralDataComponent implements OnInit, OnChanges {
  @Input() private data: any; 
  constructor() {
    console.log('mosad-general-data constructor');
  }

  ngOnInit() {
    console.log('mosad-general-data ngOnInit');

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('mosad-general-data ngOnChanges');

    if (this.data) {
      this.data = this.data[0];//todo, mock_data
    }
  }
}
