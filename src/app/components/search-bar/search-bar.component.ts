import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";
import { Observable } from "rxjs/Observable";
import { ViewContextService } from '../../services/viewcontext.service';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
   
    listMosadot: Observable<any>;
    listRashuyot: Observable<any>;
    listMechozot: Observable<any>;
   
    constructor(private dataService: DataService, public context: ViewContextService) { }

    ngOnInit() {
      this.dataService.getLists().subscribe(result => {
        this.listMosadot = result.mosadot;
        this.listRashuyot = result.rashuyot;
        this.listMechozot = result.mechozot;
      });    
    }
  
}
