import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RashutMainViewComponent } from './rashut-main-view.component';

describe('RashutMainViewComponent', () => {
  let component: RashutMainViewComponent;
  let fixture: ComponentFixture<RashutMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RashutMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RashutMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
