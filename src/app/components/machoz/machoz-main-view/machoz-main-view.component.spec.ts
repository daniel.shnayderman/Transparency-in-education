import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachozMainViewComponent } from './machoz-main-view.component';

describe('MachozMainViewComponent', () => {
  let component: MachozMainViewComponent;
  let fixture: ComponentFixture<MachozMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachozMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachozMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
