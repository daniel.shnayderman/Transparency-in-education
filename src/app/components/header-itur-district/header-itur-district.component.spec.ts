import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderIturDistrictComponent } from './header-itur-district.component';

describe('HeaderIturDistrictComponent', () => {
  let component: HeaderIturDistrictComponent;
  let fixture: ComponentFixture<HeaderIturDistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderIturDistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderIturDistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
