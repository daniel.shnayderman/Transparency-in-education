import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { NgControl } from '@angular/forms';
import { TypeaheadFilterPipe } from "../../pipes/typeahead-filter.pipe";
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute, Params} from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'itur-district',
  templateUrl: './header-itur-district.component.html',
  styleUrls: ['./header-itur-district.component.css']
})
export class HeaderIturDistrictComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute) { }
  
  @Input() mosdot: any;
  @ViewChild('instance') instance: NgbTypeahead;
  model: any;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  id: number;
  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
    });
    if (this.id) {
      this.model = this.mosdot.find(x => x.CODE_MACHOZ_GEOGRAFI === this.id);
    }
  }

  search = (text$: Observable<string>) =>
    text$
      .merge(this.focus$)
      //.merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => term.length < 2 ? []
        : this.mosdot.filter(a => this.filterfortypeahead(term, a, [
         // { key: "CODE_MACHOZ_GEOGRAFI", pos: 1 },
          { key: "SHEM_MACHOZ_GEOGRAFI", pos: 0 }
        ]) === true));


  //formatter = (x: { SHEM_MACHOZ_GEOGRAFI: string, CODE_MACHOZ_GEOGRAFI: number}) => x.SHEM_MACHOZ_GEOGRAFI + "(" + x.CODE_MACHOZ_GEOGRAFI.toString() + ")";
  formatter = (x: { SHEM_MACHOZ_GEOGRAFI: string}) => x.SHEM_MACHOZ_GEOGRAFI;

  filterfortypeahead(val, actualState, props) {

    var tempVal = val.replace(/[(|)|,]/g, " ");
    var searchTerms = tempVal.split(" ");
    if (typeof actualState === 'object') {
      for (var p = 0; p < props.length; p++) { //prop
        var key = props[p].key;
        var prefixOnly = (props[p].pos === 0);
        var i = 0;
        while (i < searchTerms.length) { //expr
          var words = actualState[key].toString().split(' ');
          var found = false;
          for (var j = 0; j < words.length; j++) {
            var idx = words[j].indexOf(searchTerms[i]);
            if (idx === 0 || (idx > 0 && !prefixOnly)) {
              found = true;
              searchTerms.splice(i, 1);
              break;
            }
          }
          //if expression not found we look for next one
          if (!found) i++;
        }
      }
    }
    return searchTerms.length === 0;
  };
  onChange() {
    if (this.model) {
      this.model.CODE_MACHOZ_GEOGRAFI ? this.router.navigate(['district', this.model.CODE_MACHOZ_GEOGRAFI]) : this.router.navigate(['district']);
    }

  }

}
