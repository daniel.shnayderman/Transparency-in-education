import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { ViewContextService } from '../../../services/viewcontext.service';

@Component({
  selector: 'app-national-main-view',
  templateUrl: './national-main-view.component.html',
  styleUrls: ['./national-main-view.component.css']
})
export class NationalMainViewComponent implements OnInit {
  generalData: any;

  public moreData: any;
  public selectedCountry: any;
  public activeButton: number = 0;
  public nationalDataToYear: { id: number, year: string }[] = [
    { "id": 0, "year": 'תשע"ו' },
    { "id": 1, "year": 'תשע"ד' },
    { "id": 2, "year": 'תשע"ג' }];

  public nationalSchoolData: { title: string, amount: string }[] = [
    { "title": 'מספר מוסדות', "amount": '4,753' },
    { "title": 'מספר תלמידים', "amount": '1,694,021' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' }];

  // public pieData1: any[] = [{
  //   name: 'מורים',
  //   y: Math.random() * 50
  // }, {
  //   name: 'מורות',
  //   y: Math.random() * 50
  // },
  // ];

  constructor(private dataService: DataService, public context: ViewContextService) {
    console.log('national-main-view constructor');
    this.dataService.getData();
  }

  ngOnInit() {
    this.selectedCountry = this.nationalDataToYear[1];
    console.log('national-main-view ngOnInit');
    this.context.mode = 'national'
    this.dataService.getNationalData(this.context.year).subscribe(result => {
      this.generalData = result;
      console.log('getNationalData');
      console.log(result);
    });

    // this.getData();
  }

  getData(): void {
    this.dataService.getData();
    console.log('here');
    console.log(this.dataService.getData());
  }
  //func(): void {
  //  this.generalData[0].sumClasses[0].KITOT_MAYUCHAD = 4526;
  //}


  setActive(value){
    this.activeButton = value;
  }



}
