import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderIturMosadComponent } from './header-itur-mosad.component';

describe('HeaderIturMosadComponent', () => {
  let component: HeaderIturMosadComponent;
  let fixture: ComponentFixture<HeaderIturMosadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderIturMosadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderIturMosadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
