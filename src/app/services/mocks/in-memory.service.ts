import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';
import * as data from "./mock_data.json";


export const InMemoryServiceConfig = { delay: 500, apiBase: "api/data", passThruUnknownUrl: true };



@Injectable()
export class InMemoryService implements InMemoryDbService {

  constructor() { }

  createDb(reqInfo?: RequestInfo) {
    return data;
  }

}
