import { Component, OnInit } from '@angular/core';
import { ViewContextService } from '../../../services/viewcontext.service';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-mosad-main-view',
  templateUrl: './mosad-main-view.component.html',
  styleUrls: ['./mosad-main-view.component.css']
})
export class MosadMainViewComponent implements OnInit {
  data: any;
  constructor(private dataService: DataService, public context: ViewContextService) {
    console.log('mosad-main-view constructor');

  };

  ngOnInit() {
    console.log('mosad-main-view ngOnInit');
    this.context.mode = 'school';
    this.context.semelMosad$
      .subscribe(semelMosad => {
        if (semelMosad !== undefined) {
          this.dataService.getMosadData(semelMosad, this.context.year).subscribe(result => {
            this.data = result;
          });
        }
      });

  }
  
  //OnMosadChanged(mosadChanged: number): void {
  //  var r = mosadChanged;
  //  this.dataService.getMosadData(mosadChanged, this.context.year).subscribe(result => {
  //          this.data = result;
  //        });
  //}

}
