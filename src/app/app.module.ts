
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ColumnChartComponent } from './charts/column-chart/column-chart.component';
import { PieChartComponent } from './charts/pie-chart/pie-chart.component';
import { DataService } from './services/data.service';
import { ViewContextService} from './services/viewcontext.service';
import { InMemoryService, InMemoryServiceConfig  } from './services/mocks/in-memory.service';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HeaderComponent } from './components/header/header.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { LocationComponent } from './components/location/location.component';
import { HttpInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TypeaheadFilterPipe } from './pipes/typeahead-filter.pipe';

import { NgbModule, NgbTypeaheadModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NationalMainViewComponent } from './components/national/national-main-view/national-main-view.component';
import { HeaderIturMosadComponent } from './components/header-itur-mosad/header-itur-mosad.component';
import { HeaderIturCityComponent } from './components/header-itur-city/header-itur-city.component';
import { HeaderIturDistrictComponent } from './components/header-itur-district/header-itur-district.component';
import { NClassesDistributionComponent } from './components/national/n-classes-distribution/n-classes-distribution.component';
import { MosadMainViewComponent } from './components/mosad/mosad-main-view/mosad-main-view.component';
import { RashutMainViewComponent } from './components/rashut/rashut-main-view/rashut-main-view.component';
import { MachozMainViewComponent } from './components/machoz/machoz-main-view/machoz-main-view.component';
import { MosadGeneralDataComponent } from './components/mosad/mosad-general-data/mosad-general-data.component';


const appRoutes: Routes = [ 
  //{ path: 'school/:id', component: HeroDetailComponent },
  //{ path: 'rashut/:id', component: HeroDetailComponent },
  { path: 'national',component: NationalMainViewComponent,data: { title: 'ארצי' }},
  { path: 'school', component: MosadMainViewComponent, data: { title: 'מוסד' } },
  { path: 'school/:id', component: MosadMainViewComponent, data: { title: 'מוסד' } },
  { path: 'city', component: RashutMainViewComponent, data: { title: 'רשות' } },
  { path: 'city/:id', component: RashutMainViewComponent, data: { title: 'רשות' } },
  { path: 'district', component: MachozMainViewComponent, data: { title: 'מחוז' } },
  { path: 'district/:id', component: MachozMainViewComponent, data: { title: 'מחוז' } },
  { path: '',redirectTo: 'national',pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ColumnChartComponent,
    PieChartComponent,
    HeaderComponent,
    SearchBarComponent,
    LocationComponent,
    TypeaheadFilterPipe,
    PageNotFoundComponent,
    NationalMainViewComponent,
    HeaderIturMosadComponent,
    HeaderIturCityComponent,
    HeaderIturDistrictComponent,
    NClassesDistributionComponent,
    MosadMainViewComponent,
    RashutMainViewComponent,
    MosadGeneralDataComponent,
    MachozMainViewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpInMemoryWebApiModule.forRoot(InMemoryService, InMemoryServiceConfig),   
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    DataService,
    ViewContextService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
