import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ViewContextService {
  mode: string = "national";
  year: number = 2017;
  //semelMosad: number = 140061;
  private _semelMosad$ = new BehaviorSubject<number>(undefined);

  constructor() {
  }

  get semelMosad$() {
    return this._semelMosad$.asObservable();
  }

  changeSemelMosad(semelMosad: number) {
    this._semelMosad$.next(semelMosad);
  }

}
