import { Component, OnInit } from '@angular/core';
import { ViewContextService } from '../../services/viewcontext.service';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
  //private context: ViewcontextService,
  constructor(private dataService: DataService, public context: ViewContextService) { }

  listShnatLimud: Observable<any>;
 

  ngOnInit() {
      this.dataService.getLists().subscribe(result => {
        this.listShnatLimud = result.years;
        console.log(this.listShnatLimud);
      });
      //this.context = "מחוז";
  }

}
