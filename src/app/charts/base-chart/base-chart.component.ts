import { Component, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import * as Highcharts from 'highcharts'

/*
// Load the 3D module.
import * as Highcharts3d from 'highcharts/highcharts-3d';
// Initialize exporting module.
Highcharts3d(Highcharts);
*/

export abstract class BaseChartComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() title: string;
  @Input() cssclass: string;
  @Input() data: any[];
  @Input() id: string;
  chart: any;

  ngOnChanges(changes: SimpleChanges): void {
    if (this.chart) this.setData();
  }

  ngAfterViewInit() {
    this.chart = Highcharts.chart(this.id, this.chartOptions());
  }

  ngOnInit() {  
  }

  abstract chartOptions(): any;

  abstract setData(): void;
}
