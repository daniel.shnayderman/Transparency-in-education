import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderIturCityComponent } from './header-itur-city.component';

describe('HeaderIturCityComponent', () => {
  let component: HeaderIturCityComponent;
  let fixture: ComponentFixture<HeaderIturCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderIturCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderIturCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
